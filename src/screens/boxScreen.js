import React from "react";
import { Text, StyleSheet, View} from "react-native";

const BoxScreen = () => {

    return (
        <View style = {styles.viewStyle}>
            <Text style= {styles.textOneStyle}>Child 1</Text>
            <Text style={styles.textTwoStyle}>Child 2</Text>
            <Text style={styles.textThreeStyle}>Child 3</Text>
            <Text style={styles.textFourStyle}>Child 4</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    viewStyle: {
        borderWidth: 4,
        borderColor: 'black',
        justifyContent: 'center',
        height: 300
    },
    textOneStyle: {
        borderWidth: 3,
        borderColor: 'red',
        alignSelf: 'flex-start',
        width: 150,
        fontSize: 22
    },
    textTwoStyle: {
        borderWidth: 3,
        borderColor: 'red',
        flex: 2,
        ...StyleSheet.absoluteFillObject
    },
    textThreeStyle: {
        borderWidth: 3,
        borderColor: 'red',
        position: "absolute",
        flex: 1,
        fontSize: 13
    },
    textFourStyle: {
        borderWidth: 3,
        borderColor: 'red',
        flex: 1,
        fontSize: 13,
        left: 32
    }
});

export default BoxScreen;
import React, { useState } from "react";
import { Text, StyleSheet, View, TextInput } from "react-native";

const TextScreen = () => {

    const [name, setName] = useState('');

    return (
        <View>
            <Text>Enter Name:</Text>
            <TextInput
                style = {styles.input}
                autoCapitalize={"none"} 
                autoCorrect = {false}
                value = {name}
                onChangeText={(newValue) => setName(newValue) }
            />
            {name.length > 5 ? <Text>Accepted Password</Text> : <Text>Password must be lomger than 5 characters to be accepted</Text>}
        </View>
    );
};

const styles = StyleSheet.create({
    input : {
        margin: 15,
        borderColor: 'black',
        borderWidth: 1
    }
});

export default TextScreen;
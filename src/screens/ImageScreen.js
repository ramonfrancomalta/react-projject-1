import React from "react";
import { Text, StyleSheet, View} from "react-native";
import ImageDetail from "../components/imageDetail";

const ImageScreen = ({ navigation }) => {
    return (
        <View>
            <ImageDetail title="Forest" imageSource={require('../../assets/forest.jpg')} imageScore = '9'/>
            <ImageDetail title="Mountain" imageSource={require('../../assets/beach.jpg')} imageScore='6'/>
            <ImageDetail title="Beach" imageSource={require('../../assets/mountain.jpg')} imageScore='7'/>
            <ImageDetail title="City" imageSource={require('../../assets/forest.jpg')} imageScore='0'/>
        </View>
    );
};

const styles = StyleSheet.create({});

export default ImageScreen;
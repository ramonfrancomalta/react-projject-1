import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const ComponentScreen = () => {

    const name = 'Ramón';

    return( 
        <View>
            <Text style={styles.textStyleTopic } > Getting started with react native! </Text> 
            <Text style={styles.textStyleText}> My name is {name}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    textStyleTopic: {
        fontSize: 45
    },
    textStyleText: {
        fontSize: 20
    }
});



export default ComponentScreen;
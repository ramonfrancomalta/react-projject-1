import React, {useState} from "react";
import { Text, StyleSheet, View, Button, FlatList } from "react-native";

const ColorScreen = (props) => {

    const [colors, setColors] = useState([]);
    return (
        <View>
            <Button title="Add a Color" onPress={() => {
                setColors([...colors, randoRGB()])
            }}></Button>
            
            <FlatList
            keyExtractor={(item) => item}
                data={colors}
                renderItem={({item}) => {
                    return <View style={{ height: 100, width: 100, backgroundColor: item }}></View>;
                }}
            />
        </View>
    );
};

const randoRGB = () =>{
    const r = Math.floor(Math.random() * 256);  
    const g = Math.floor(Math.random() * 256); 
    const b = Math.floor(Math.random() * 256); 

    return `rgb(${r}, ${g}, ${b})`;
};

const styles = StyleSheet.create({});

export default ColorScreen;
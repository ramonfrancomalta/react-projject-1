import React, { useReducer } from "react";
import { Text, StyleSheet, View, Button, FlatList } from "react-native";
import ColorCounter from "../components/ColorCounter";

const ColorIncrement = 10;

const reducer = (state, action) => {

    switch(action.type){
        case 'change_red':
            return state.red + action.payload > 255 || state.red + action.amount < 0
                    ? state
                : { ...state, red: state.red + action.payload};
        case 'change_green':
            return state.green + action.payload > 255 || state.green + action.payload < 0
                    ? state :
                { ...state, green: state.green + action.payload };
        case 'change_blue':
            return state.blue + action.payload > 255 || state.blue + action.payload < 0
                    ? state 
                : { ...state, blue: state.blue + action.payload };
        default:
    }
}

const SquareScreen = (props) => {
    const [state, dispatch] = useReducer(reducer, {red: 0, green: 0, blue:0});
    const { red, green, blue} = state;
    return (
        <View>
            <ColorCounter 
                onIncrease={() => dispatch({ type: 'change_red', payload: ColorIncrement})}  
                onDecrease={() => dispatch({ type: 'change_red', payload: -ColorIncrement })} 
                color='Red' />
            <ColorCounter 
                onIncrease={() => dispatch({ type: 'change_green', payload: ColorIncrement })} 
                onDecrease={() => dispatch({ type: 'change_green', payload: -ColorIncrement })} 
                color='Green' />
            <ColorCounter 
                onIncrease={() => dispatch({ type: 'change_blue', payload: ColorIncrement })} 
                onDecrease={() => dispatch({ type: 'change_blue', payload: -ColorIncrement })} 
                color='Blue' />
            <View style={{ height: 100, width: 100, backgroundColor: `rgb(${red}, ${green}, ${blue})`}}></View>
        </View>
    );
};


const styles = StyleSheet.create({});

export default SquareScreen;